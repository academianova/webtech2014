package logic;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import data.Product;
import data.ProductDAO;

public class ProductService {
	
	private ProductDAO dao;

	public ProductDAO getDao() {
		return dao;
	}

	public void setDao(ProductDAO dao) {
		this.dao = dao;
	}
	
	@Transactional(readOnly=true)
	public List<Product> getAllProducts() {
		List<Product> list = dao.getAllProducts();
		return list;
	}
	
	@Transactional(readOnly=true)
	public Product getOneProduct(long id){
		Product productdetail = dao.getOneProduct(id);
		return productdetail;
	}
	
	@Transactional(readOnly=true)
	public boolean enoughOnStock(long id, int ordered) {
		Product product = dao.getOneProduct(id);
		return product.getLager() >= ordered;
	}
	
	@Transactional
	public void reduceStock(Cart cart) {
		Product missing = cart.missingProduct(this);
		if (missing != null) {
			throw new MissingProductException(missing);
		}		
		for (BestellForm item : cart.getItems()) {
			dao.reduceStock(item.getId(), item.getMenge());
		}
	}
}
