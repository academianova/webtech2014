package logic;

import data.Product;

public class MissingProductException extends RuntimeException {
	
	private Product product;
	
	public MissingProductException(Product p) {
		product = p;
	}
	
	public Product getProduct() {
		return product;
	}

}
