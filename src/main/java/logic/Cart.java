package logic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import data.Product;
import web.LineItem;

public class Cart {
	
	private Logger log = LoggerFactory.getLogger(Cart.class);

	public static final String CART_NAME = "cart";
	private List<BestellForm> items;
	
	private Cart() {
		items = new ArrayList<BestellForm>();
	}

	static public Cart getCart(HttpSession session) {
		Cart cart = (Cart) session
				.getAttribute(CART_NAME);
		if (cart == null) {
			cart = new Cart();
			session.setAttribute(CART_NAME, cart);
		}
		return cart;
	}
	
	public List<BestellForm> getItems() {
		return items;
	}
	
	public double totalPrice(ProductService ps) {
		double total= 0;
		for (BestellForm b:items) {
			Product p= ps.getOneProduct(b.getId());
			LineItem line= new LineItem(b.getId(), p.getName(), p.getPreis(), b.getMenge(), p.getBild());
			total= total+line.getPxm();
		}
		return total;
	}
	
	public BestellForm findItem(long productId) {
		Iterator<BestellForm> it = items.iterator();
		while (it.hasNext()) {
			BestellForm b = it.next();
			if (b.getId() == productId) {
				return b;
			}
		}
		return null;
	}

	public void removeProduct(long productId) {
		Iterator<BestellForm> it = items.iterator();
		while (it.hasNext()) {
			BestellForm b = it.next();
			if (b.getId() == productId) {
				it.remove();
				break;
			}
		}
	}
	
	public void changeAmount(long productId, int amount) {
		for (BestellForm b:items) {
			if (b.getId() == productId){
				b.setMenge(amount);
				break;
			}
		}
	}
	
	public Product missingProduct(ProductService ps) {
		long id = 0;
		for(BestellForm b:items)	{
			if (!ps.enoughOnStock(b.getId(), b.getMenge())) {
				id = b.getId();
				log.debug("not enough on stock {}", b.getId());
				return ps.getOneProduct(id);
			}
		}
		return null;
	}
	
	public void clear() {
		items.clear();
	}
}
