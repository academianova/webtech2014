package web;

public class LineItem {

	
	private long id;
	private String name;
	private double preis;
	private int menge;
	private String bild;
	
	
	public LineItem(long id, String name, double preis, int menge, String bild){
		this.id = id;
		this.name = name;
		this.preis = preis;
		this.menge = menge;
		this.bild = bild;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPreis() {
		return preis;
	}
	public void setPreis(double preis) {
		this.preis = preis;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	public String getBild() {
		return bild;
	}
	public void setBild(String bild) {
		this.bild = bild;
	}
	public double getPxm() {
		return menge*preis;
	}
	
}
