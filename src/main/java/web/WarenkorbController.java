package web;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSession;

import logic.BestellForm;
import logic.Cart;
import logic.MissingProductException;
import logic.ProductService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import data.Product;

@Controller
public class WarenkorbController {
	
	private Logger log = LoggerFactory.getLogger(WarenkorbController.class);
	
	@Autowired
	private ProductService ps;
	
	@RequestMapping(value="warenkorb", method=RequestMethod.GET) 
	public ModelAndView showWarenkorb(HttpSession session) { 
		List <LineItem> lineItems = new LinkedList<LineItem>();
		Cart cart = Cart.getCart(session);
		List <BestellForm> bestellung= cart.getItems();
		double total= cart.totalPrice(ps);
		for (BestellForm b:bestellung) {
			Product p= ps.getOneProduct(b.getId());
			LineItem line= new LineItem(b.getId(), p.getName(), p.getPreis(), b.getMenge(), p.getBild());
			lineItems.add(line);
		}

		ModelAndView mav = new ModelAndView("Warenkorb"); 
		mav.addObject("lineitems", lineItems);
		mav.addObject("total", total);
		return mav; 
	} 
	
	@RequestMapping(value="warenkorb", method=RequestMethod.POST, params="change")
	public ModelAndView processChange(HttpSession session, @RequestParam("change") long productId) {
		log.debug("change amount");
		ModelAndView mav = new ModelAndView("WKbearbeiten");
		Product details = ps.getOneProduct (productId);
		mav.addObject("detail", details);
		Cart cart = Cart.getCart(session);
		mav.addObject("lineitem", cart.findItem(productId));
		return mav;
	}
	
	@RequestMapping(value="warenkorb", method=RequestMethod.POST, params="delete")
	public ModelAndView processDelete(HttpSession session, @RequestParam("delete") long productId) {
		log.debug("delete item {}", productId);
		Cart cart = Cart.getCart(session);
		cart.removeProduct(productId);
		return new ModelAndView("redirect:warenkorb");
	}
	
	@RequestMapping(value="menge", method=RequestMethod.POST)
	public ModelAndView changeMenge(HttpSession session, BestellForm form, BindingResult result, @RequestParam("id") long productId) {
		if (result.hasErrors()) {
			log.debug("form has errors {}", result);
			Product details = ps.getOneProduct (productId);
			ModelAndView mav = new ModelAndView("Details");
			mav.addObject(BindingResult.MODEL_KEY_PREFIX + "form", result);
			mav.addObject("detail", details);
			return mav;
		} else {
			Cart cart = Cart.getCart(session);
			cart.changeAmount(form.getId(), form.getMenge());
		}
		ModelAndView mav = new ModelAndView("redirect:warenkorb");
		return mav;
	}
	
	@RequestMapping(value="checkout", method=RequestMethod.GET)
	public ModelAndView confirmCheckout(HttpSession session) {
		Cart cart = Cart.getCart(session);
		Product missingProduct = cart.missingProduct(ps);
		if(missingProduct != null){
			ModelAndView mav = new ModelAndView("LagerFehler");
			mav.addObject("product", missingProduct);
			return mav;
		}
		else {
			double total= cart.totalPrice(ps);
			log.debug("total {}", total);
			ModelAndView mav = new ModelAndView("Confirmation");
			mav.addObject("total", total);
			return mav;
		}
	}
	
	@RequestMapping(value="confirm", method=RequestMethod.POST, params="pay")
	public ModelAndView confirmExit(HttpSession session) {
		Cart cart = Cart.getCart(session);
		try {
			ps.reduceStock(cart);
			cart.clear();
			return new ModelAndView ("Danke");			
		} catch (MissingProductException e) {
			ModelAndView mav = new ModelAndView("LagerFehler");
			mav.addObject("product", e.getProduct());
			return mav;
		}
	}
	
	@RequestMapping(value="confirm", method=RequestMethod.POST, params="exit")
	public ModelAndView confirmExit() {
		return new ModelAndView ("redirect:warenkorb");
	}

}
