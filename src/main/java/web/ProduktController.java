package web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Controller; 
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.servlet.ModelAndView; 




import data.Product; 
import logic.BestellForm;
import logic.Cart;
import logic.ProductService; 

@Controller public class ProduktController { 
	
	private Logger log= LoggerFactory.getLogger(ProduktController.class);
	
	@Autowired ProductService produktService; 
	
	@RequestMapping(value="produkt", method=RequestMethod.GET) 
	
	public ModelAndView showProdukte() { 
		List <Product> produkt = produktService.getAllProducts(); 
		ModelAndView mav = new ModelAndView("Produkt"); 
		mav.addObject("produkte", produkt); 
		return mav; 
	} 

	@RequestMapping(value="produkt/{productId}", method=RequestMethod.GET) 
	
	public ModelAndView showDetail(@PathVariable long productId) {
		Product details = produktService.getOneProduct (productId);
		ModelAndView mav = new ModelAndView("Details");
		mav.addObject("detail", details);
		BestellForm form = new BestellForm();
		form.setId(productId);
		form.setMenge(1);
		mav.addObject("form", form);
		return mav;
	}
	
	@RequestMapping(value="produkt/{productId}", method=RequestMethod.POST)
	public ModelAndView processOrder(HttpSession session, BestellForm form, BindingResult result, @PathVariable long productId) {
		if (result.hasErrors()) {
			log.debug("form has errors {}", result);
			return buildErrorModel(result, productId);
		} else {
			Cart cart = Cart.getCart(session);
			List <BestellForm> bestellung= cart.getItems();
			if(form.getMenge() <= 0 ){
				
				result.rejectValue("menge", "toolow", "too low");
				return buildErrorModel(result, productId);
			}
			addFormToBestellung (bestellung, form);
			ModelAndView mav = new ModelAndView("redirect:../warenkorb");
			return mav;
		}
	}

	private void addFormToBestellung(List<BestellForm> bestellung,
			BestellForm form) {
		boolean found = false;
		for (BestellForm b:bestellung) {
			if (b.getId() == form.getId()){
				b.setMenge(b.getMenge() + form.getMenge());
				found = true;
				break;
			}
		}
		if (!found) {
			bestellung.add(form);
		}
	}

	private ModelAndView buildErrorModel(BindingResult result, long productId) {
		Product details = produktService.getOneProduct (productId);
		ModelAndView mav = new ModelAndView("Details");
		mav.addObject(BindingResult.MODEL_KEY_PREFIX + "form", result);
		mav.addObject("detail", details);
		return mav;
	}

}
