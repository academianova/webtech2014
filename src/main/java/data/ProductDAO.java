package data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class ProductDAO {

	private JdbcTemplate jt;
	
	private Logger log = LoggerFactory.getLogger(ProductDAO.class);

	public void setDataSource(DataSource ds) {
		jt = new JdbcTemplate(ds);
	}

	private static class ProductMapper implements RowMapper<Product> {

		@Override
		public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
			Product p = new Product();
			p.setId(rs.getLong(1));
			p.setName(rs.getString(2));
			p.setPreis(rs.getDouble(3));
			p.setLager(rs.getInt(4));
			p.setBeschreibung(rs.getString(5));
			p.setBild(rs.getString(6));
			return p;
		}
	}

	public List<Product> getAllProducts() {
		List<Product> list = jt
				.query("SELECT id, name, preis, lagerbestand, beschreibung, bild FROM Produkt ORDER BY id",
						new ProductMapper());

		return list;
	}

	public Product getOneProduct(long id) {
		Product p = jt
				.queryForObject(
						"SELECT id, name, preis, lagerbestand, beschreibung, bild FROM Produkt WHERE id = ?",
						new Object[] { id }, new ProductMapper());
		return p;
	}
	

	
	public void reduceStock(long id, int menge){
		
		
		int i = jt
				.update("UPDATE Produkt SET lagerbestand = lagerbestand - ?  WHERE id = ?",  
		
				new Object[] { menge, id });
		if (i != 1) {
			log.error("update error occurred at {}", id);
		}
	}
	
}
