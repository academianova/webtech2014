<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Warenkorb bearbeiten</title>
</head>
<body>
<h1>Hier k&ouml;nnen Sie die Menge des Produktes bearbeiten:</h1>
<form:form action="menge" modelAttribute="lineitem">
<form:input type="hidden" path="id"/>
<table>
	 <tr>
	 	<td><img src="${detail.bild}" alt="${detail.name}" width="80"></a>
	 	<td colspan="3"><c:out value="${detail.name}"/>
	 </tr>
	<tr>
		<td>Aktuelle Menge: <c:out value="${lineitem.menge}"/>
		<td>Bestellmenge: <form:input path="menge"/></td>
		<td><button type="submit">&Auml;ndern</button></td>
		<td><form:errors cssClass="error" path="menge" />
	</tr>
	<tr>
		<td><a href="warenkorb">Zum Warenkorb</a>
	</tr>
</table>
</form:form>
</body>
</html>