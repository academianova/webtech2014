<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lagerbestand</title>
</head>
<body>
<h1>Fehler beim Lagerbestand</h1>
<p> Zurzeit liegt leider ein Engpass bei <c:out value="${product.name}"/> vor.
<p> Es gibt nur noch <c:out value="${product.lager}"/> St&uuml;ck auf Lager.
<a href="warenkorb">zum Warenkorb</a>
</body>
</html>