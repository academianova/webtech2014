<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Produktliste</title>
</head>
<body>
<h1>Willkommen im Zauberland</h1>
<table>
	<c:forEach items="${produkte}" var="produkt">
	 <tr>
	 	<td><a href="produkt/${produkt.id}"><img src="${produkt.bild}" alt="${produkt.name}" width="80"></a>
	 	<td><a href="produkt/${produkt.id}"><c:out value="${produkt.name}"/></a>
	 	<br><c:out value="${produkt.beschreibung}"/>
	 	<td><c:out value="${produkt.preis}"/>
	 </tr>
	</c:forEach>
</table>
<a href="warenkorb">zum Warenkorb</a>
</body>
</html>