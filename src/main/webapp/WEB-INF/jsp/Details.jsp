<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Details</title>
</head>
<body>
<h1>Hier finden Sie die Details zu dem ausgew&auml;hlten Produkt:</h1>
<form:form modelAttribute="form">
<form:input type="hidden" path="id"/>
<table>
	 <tr>
	 	<td><img src="${detail.bild}" alt="${detail.name}" width="300"></a>
	 	<td colspan="3"><c:out value="${detail.name}"/>
	 	<br><c:out value="${detail.beschreibung}"/>
	 	<br>EUR <c:out value="${detail.preis}"/>
	 </tr>
	<tr>
		<td>Lagerbestand: <c:out value="${detail.lager}" />
		<td>Bestellmenge: <form:input path="menge"/></td>
		<td><button type="submit">In den Warenkorb</button></td>
		<td><form:errors cssClass="error" path="menge" />
	</tr>
	<tr>
		<td><a href="../produkt">Produktliste</a>
	</tr>
</table>
</form:form>
</body>
</html>