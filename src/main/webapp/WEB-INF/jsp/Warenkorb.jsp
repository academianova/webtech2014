<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Warenkorb</title>
</head>
<body>
<h1>Ihr Warenkorb:</h1>
<form action="warenkorb" method="post">
<table>
	 <tr>
	 	<th> Bild
	 	<th> Name
	 	<th> Menge
	 	<th> Einzelpreis
	 	<th> Summe
	 	<th colspan="2">
	 </tr>
	 <c:forEach items = "${lineitems}" var="lineitem">
	 <tr>
	 	<td><img src="${lineitem.bild}" alt="${lineitem.name}" width="80">
	 	<td><c:out value="${lineitem.name}"/>
	 	<td><c:out value="${lineitem.menge}"/>
	 	<td>EUR <fmt:formatNumber value="${lineitem.preis}" type="number" maxFractionDigits="2" />
	 	<td>EUR <fmt:formatNumber value="${lineitem.pxm}" type="number" maxFractionDigits="2" />
	 	<td><button type="submit" name="change" value="${lineitem.id}">Menge &auml;ndern</button></td>
	 	<td><button type="submit" name="delete" value="${lineitem.id}">L&ouml;schen</button></td>
	</tr>
	</c:forEach>
	<tr>
		<td colspan=3>
		<td>Gesamtpreis: 
		<td>EUR <fmt:formatNumber value="${total}" type="number" maxFractionDigits="2" />
	 	<td colspan="2">
	</tr>
	<tr>
		<td colspan=4> <a href="produkt">zur&uuml;ck </a>
		<td>
			<c:if test="${total > 0}">
				<a href="checkout">checkout</a>
			</c:if>
	 	<td colspan="2">
	</tr>
</table>
</form>

</body>
</html>