<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Confirmation</title>
</head>
<body>
<h1>Best&auml;tigung</h1>
<p> Ihr zu zahlender Betrag ist wie folgt:
<p>EUR <fmt:formatNumber value="${total}" type="number" maxFractionDigits="2" />
<form action="confirm" method="post">
<button type="submit" name="exit">zur&uuml;ck</button>
<button type="submit" name="pay" >zahlen</button>
</form>
</body>
</html>